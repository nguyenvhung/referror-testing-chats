//
//  AppDelegate.swift
//  referror_chating_test
//
//  Created by Nguyen Van Hung on 04/05/2023.
//

import UIKit
import MirrorFlySDK

let BASE_URL = "https://api-preprod-sandbox.mirrorfly.com/api/v1/"
let LICENSE_KEY = "9CeoZ8NOroxJJsvTFbQxTRKldGorPG"
let CONTAINER_ID = "group.hungnguy.mirrorfly.quickstart"

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        try? ChatSDK.Builder.setAppGroupContainerID(containerID: CONTAINER_ID)
              .setLicenseKey(key: LICENSE_KEY)
              .isTrialLicense(isTrial: true)
              .setDomainBaseUrl(baseUrl: BASE_URL)
              .buildAndInitialize()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

