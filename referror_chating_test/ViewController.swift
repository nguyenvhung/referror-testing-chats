//
//  ViewController.swift
//  referror_chating_test
//
//  Created by Nguyen Van Hung on 04/05/2023.
//

import UIKit
import MirrorFlySDK
import SnapKit

typealias MirrorFlyConnectionEventDelegate = ConnectionEventDelegate
typealias MirrorFlyMessageEventsDelegate = MessageEventsDelegate

class ViewController: UIViewController {
    var userIdentifierTextField: UITextField = {
        var txtfield = UITextField()
        txtfield.backgroundColor = .white
        txtfield.textColor = .black
        txtfield.textAlignment = .center
        return txtfield
    }()
    
    var uiButton: UIButton = {
        var button = UIButton()
        button.setTitle("Register", for: .normal)
        button.tintColor = .blue
        return button
    }()
    
    var sendButton: UIButton = {
        var button = UIButton()
        button.setTitle("Send", for: .normal)
        button.tintColor = .blue
        return button
    }()
    
    var USER_IDENTIFIER: String?
    var username: String?
    var password: String?
    var selfJid: String?
    
    //    let mirrorFlyChatManager = ChatManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .gray
        
        //        self.autoRegister()
        
        self.view.addSubview(self.uiButton)
        self.view.addSubview(self.sendButton)
        self.view.addSubview(self.userIdentifierTextField)
        self.uiButton.snp.makeConstraints { make in
            make.width.equalTo(100)
            make.height.equalTo(24)
            make.center.equalToSuperview()
        }
        self.userIdentifierTextField.snp.makeConstraints { make in
            make.width.equalTo(200)
            make.height.equalTo(24)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.uiButton.snp.top).offset(-16)
        }
        self.sendButton.snp.makeConstraints { make in
            make.width.equalTo(100)
            make.height.equalTo(24)
            make.centerX.equalToSuperview()
            make.top.equalTo(self.uiButton.snp.bottom).offset(16)
        }
        
        self.uiButton.addTarget(self, action: #selector(registerMirrorFlyUser), for: .touchUpInside) // Work!
        self.sendButton.addTarget(self, action: #selector(sendMessageMirrorFly), for: .touchUpInside)
        
        self.sendButton.isHidden = !(self.selfJid != nil && self.username != nil && self.password != nil)
        
        self.userIdentifierTextField.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        ChatManager.disconnect()
    }
    
    private func autoRegister() {
        let seconds = 4.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            self.registerMirrorFlyUser() // Work!
        }
    }
    
    @objc private func registerMirrorFlyUser() {
        guard let userId = self.USER_IDENTIFIER else {
            showAlertView()
            return
        }
        
        try! ChatManager.registerApiService(for:  userId) { isSuccess, flyError, flyData in
            var data = flyData
            if isSuccess {
                // This is your Password
                let password = data["password"] as? String
                
                // This is your Username
                let username = data["username"] as? String
                debugPrint(">>> username = \(username) and password is = \(password)")
                
                self.username = username
                self.password = password
                
                self.getJid()
                
            } else {
                let error = data.getMessage()
                print("#chatSDK \(error)")
            }
        }
    }
    
    
    private func getJid() {
        guard let username = self.username else {
            debugPrint(">>> get Jid failed because username is null!")
            return
        }
        do {
            self.selfJid = try FlyUtils.getJid(from: username)
            self.sendButton.isHidden = !(self.selfJid != nil && self.username != nil && self.password != nil)
        } catch {
            debugPrint(">>> get Jid failed!")
            return
        }
        
        ChatManager.shared.connectionDelegate = self
        ChatManager.shared.messageEventsDelegate = self
        ChatManager.connect()
    }
    
    @objc private func sendMessageMirrorFly() {
        let message: String = randomWord(wordLength: 10)
        let destination: String? = try? FlyUtils.getJid(from: "71")
        
        guard let receiver = destination else {
            debugPrint(">>> Cannot get JID of receiver)")
            return
        }
        
        FlyMessenger.sendTextMessage(toJid: receiver, message: message) { isSuccess, error, chatMessage in
            debugPrint(">>> MirrorFly sending message: success = \(isSuccess)")
            
            if let err = error {
                debugPrint(">>> MirrorFly error = \(err.localizedDescription)")
            } else {
                debugPrint(">>> MirrorFly sent \(chatMessage)!")
            }
         }
    }
    
    private func showAlertView() {
        let alert = UIAlertController(title: "Alert", message: "Please input user identifier",
                                      preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            alert.dismiss(animated: true)
        })
        alert.addAction(yesAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.USER_IDENTIFIER = textField.text
        return true
    }
}

extension ViewController: MirrorFlyConnectionEventDelegate {
    func onConnected() {
        debugPrint(">>> MirrorFly connected!")
    }
    
    func onDisconnected() {
        debugPrint(">>> MirrorFly disconnected!")
    }
    
    func onConnectionNotAuthorized() {
        debugPrint(">>> MirrorFly onConnectionNotAuthorized!")
    }
    
}

extension ViewController: MirrorFlyMessageEventsDelegate {
    func onMessageReceived(message: ChatMessage, chatJid: String) {
          // Message received from chatJid, content is message.messageTextContent
        debugPrint(">>> Message Received: \(message.messageTextContent); jid = \(chatJid)")
    }
    
    func onMessageStatusUpdated(messageId: String, chatJid: String, status: MirrorFlySDK.MessageStatus) {
         
    }
    
    func onMediaStatusUpdated(message: MirrorFlySDK.ChatMessage) {
         
    }
    
    func onMediaStatusFailed(error: String, messageId: String) {
         
    }
    
    func onMediaProgressChanged(message: MirrorFlySDK.ChatMessage, progressPercentage: Float) {
         
    }
    
    func onMessagesClearedOrDeleted(messageIds: Array<String>) {
         
    }
    
    func onMessagesDeletedforEveryone(messageIds: Array<String>) {
         
    }
    
    func showOrUpdateOrCancelNotification() {
         
    }
    
    func onMessagesCleared(toJid: String, deleteType: String?) {
         
    }
    
    func setOrUpdateFavourite(messageId: String, favourite: Bool, removeAllFavourite: Bool) {
         
    }
    
    func onMessageTranslated(message: MirrorFlySDK.ChatMessage, jid: String) {
         
    }
    
    func clearAllConversationForSyncedDevice() {
         
    }
    
}
